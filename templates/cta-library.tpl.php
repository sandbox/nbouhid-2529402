<?php 
/**
 * @file
 * Default implementation for cta library button
 */
?>
<a class="cta-library-wrap <?php print $term_name; ?>" href="<?php print $link; ?>">
  <?php if (!empty($icon)): ?>
    <span class="cta-library-icon"><?php print $icon; ?></span>
  <?php endif;?>  
  <span class="cta-library-label"><?php print $text; ?></span>
</a>
