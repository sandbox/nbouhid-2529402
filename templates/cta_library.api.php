<?php

/**
 * @file
 * Hooks provided by the cta_library module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow modules to override the icon renderable array.
 *
 * @param array $icon_renderable_array
 *   The renderable array for the icon
 * @param string $field_name
 *   The name of the field.
 */
function hook_motorola_language_settings_alter(&$icon_renderable_array, $field_name) {
  if ($field_name != 'field_my_field') {
    return;
  }

  // Override renderable array settings.
  $icon_renderable_array['#theme'] = 'picture';
}

/**
 * @} End of "addtogroup hooks".
 */
