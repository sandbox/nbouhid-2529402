<?php
/**
 * @file
 * fs_cta_library.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fs_cta_library_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cta_library_cta_settings|taxonomy_term|cta_types|form';
  $field_group->group_name = 'group_cta_library_cta_settings';
  $field_group->entity_type = 'taxonomy_term';
  $field_group->bundle = 'cta_types';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_cta_library';
  $field_group->data = array(
    'label' => 'CTA settings',
    'weight' => '44',
    'children' => array(
      0 => 'field_background_color',
      1 => 'field_border_color',
      2 => 'field_link_color',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cta-library-cta-settings field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cta_library_cta_settings|taxonomy_term|cta_types|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cta_library_general|taxonomy_term|cta_types|form';
  $field_group->group_name = 'group_cta_library_general';
  $field_group->entity_type = 'taxonomy_term';
  $field_group->bundle = 'cta_types';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_cta_library';
  $field_group->data = array(
    'label' => 'General',
    'weight' => '43',
    'children' => array(
      0 => 'field_icon',
      1 => 'metatags',
      2 => 'path',
      3 => 'name',
      4 => 'description',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cta-library-general field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cta_library_general|taxonomy_term|cta_types|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cta_library_settings_hover|taxonomy_term|cta_types|form';
  $field_group->group_name = 'group_cta_library_settings_hover';
  $field_group->entity_type = 'taxonomy_term';
  $field_group->bundle = 'cta_types';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_cta_library';
  $field_group->data = array(
    'label' => 'CTA settings (rollover)',
    'weight' => '45',
    'children' => array(
      0 => 'field_background_color_hover',
      1 => 'field_border_color_hover',
      2 => 'field_link_color_hover',
    ),
    'format_type' => 'tab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-cta-library-settings-hover field-group-tab',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_cta_library_settings_hover|taxonomy_term|cta_types|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cta_library|taxonomy_term|cta_types|form';
  $field_group->group_name = 'group_cta_library';
  $field_group->entity_type = 'taxonomy_term';
  $field_group->bundle = 'cta_types';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Groups wrapper',
    'weight' => '43',
    'children' => array(
      0 => 'group_cta_library_cta_settings',
      1 => 'group_cta_library_settings_hover',
      2 => 'group_cta_library_general',
    ),
    'format_type' => 'tabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-cta-library field-group-tabs',
      ),
    ),
  );
  $export['group_cta_library|taxonomy_term|cta_types|form'] = $field_group;

  return $export;
}
